package gelato;

import gelato.mail.*;
import gelato.mail.types.*;
import gelato.wetter.IWetterstation;
import gelato.wetter.Wetter;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import static java.time.DayOfWeek.*;

public class MarketingLogic {

    private final IWetterstation wetterstation;

    public MarketingLogic(IWetterstation wetterstation) {
        this.wetterstation = wetterstation;
    }

    public Mail createMail(String name) {
        Wetter wetter = wetterstation.wetterDaten();
        LocalDateTime now = LocalDateTime.now();
        DayOfWeek tag = now.getDayOfWeek();
        boolean early = now.getHour() > 6 && now.getHour() < 10;

        // Wetter Logic
        if (wetter.istHeiss() && wetter.istTrocken()) {
            if (early) {
                return new Granita(name).generateMail();
            }
            return new Gelato(name).generateMail();
        }
        else if (wetter.istKalt()) {
            return new WaffelUndKaffee(name).generateMail();
        }
        else {
            if (tag.equals(SATURDAY) || tag.equals(SUNDAY)) {
                return new ZuppaInglese(name).generateMail();
            }
            if (tag.equals(MONDAY) || tag.equals(WEDNESDAY)) {
                return new Cassata(name).generateMail();
            }
            if (tag.equals(TUESDAY) || tag.equals(THURSDAY)) {
                return new Tiramisu(name).generateMail();
            }
            if (tag.equals(FRIDAY)) {
                return new PannaCotta(name).generateMail();
            }
        }
        return new Mail("", "");
    }
}
