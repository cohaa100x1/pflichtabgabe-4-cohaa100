package gelato.mail;

import gelato.kunde.Kunde;

public interface MailServer {

    void sendMail(Kunde kunde);

}
