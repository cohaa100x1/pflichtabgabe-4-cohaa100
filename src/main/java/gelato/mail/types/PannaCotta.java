package gelato.mail.types;


public class PannaCotta extends MailArt {

    public PannaCotta(String name) {
        super(name);
    }

    @Override
    public String getBody() {
        return String.format("Hallo %s,%n%nendlich ist Freitag! " +
                "Und zur Feier des Tages haben wir unsere hausgemachte Panna Cotta im Angebot.%n\n" +
                "Du hast die freie Wahl, ob du die original Panna Cotta mit herrlich frischem Kompptt " +
                "oder Caramellsoße haben willst.%n%nDas Angebot gilt nur solange unser Vorrat reicht.%n%n" +
                "Wir freuen uns auf Dich!%nDein Team von Gelateria Giacomo%n", getName());
    }

    @Override
    public String getSubject() {
        return "Es gibt hausgemachte Panna Cotta";
    }

}
