package gelato.mail.types;


public class WaffelUndKaffee extends MailArt {

    public WaffelUndKaffee(String name) {
        super(name);
    }

    @Override
    public String getBody() {
        return String.format("Hallo %s,%n%n" +
                "lass dich vom Regen und der Kälte nicht runterziehen. Wir haben genau das Richtige! Leckere hausgemachte Waffeln und einen großen Pott Kaffee.%n%n" +
                "%n%nKomm einfach vorbei und nimm eine Auszeit.%n%n" +
                "Wir freuen uns auf Dich!%nDein Team von Gelateria Giacomo%n", getName());
    }

    @Override
    public String getSubject() {
        return "Mistwetter? Egal!";
    }

}
