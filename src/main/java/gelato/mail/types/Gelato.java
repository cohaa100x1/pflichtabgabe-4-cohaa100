package gelato.mail.types;



public class Gelato extends MailArt {

    public Gelato(String name) {
        super(name);
    }

    @Override
    public String getBody() {
        return String.format("Hallo %s,%n%n" +
                "ist dir auch so heiß? Wir haben genau das Richtige! Leckeres hausgemachtes Eis in Meisterqualität.%n%n" +
                "%n%nKomm einfach vorbei und kühl dich bei einem original italienischem Gelato ab.%n%n" +
                "Wir freuen uns auf Dich!%nDein Team von Gelateria Giacomo%n", getName());
    }

    @Override
    public String getSubject() {
        return "Warm! Warm! Warm!";
    }

}
