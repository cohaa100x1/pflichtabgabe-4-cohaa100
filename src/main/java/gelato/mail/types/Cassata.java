package gelato.mail.types;

public class Cassata extends MailArt {

    public Cassata(String name) {
        super(name);
    }

    @Override
    public String getBody() {
        return String.format("Hallo %s,%n%n" +
                "unser hausgemachtes Cassata Eis ist der original Cassata alla siciliana nachempfunden. Sie besteht aus  Himbeer-, Vanille- und Schokoladeneis sowie kandierten Früchten%n%n" +
                "%n%nDas Angebot gilt nur solange unser Vorrat reicht.%n%n" +
                "Wir freuen uns auf Dich!%nDein Team von Gelateria Giacomo%n", getName());
    }

    @Override
    public String getSubject() {
        return "Die Bombe aus Italien: Cassata";
    }

}
