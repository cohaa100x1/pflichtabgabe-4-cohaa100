package gelato.mail.types;


public class Granita extends MailArt {

    public Granita(String name) {
        super(name);
    }

    @Override
    public String getBody() {
        return String.format("Hallo %s,%n%n" +
                "Wenn es in Sizilien warm ist, dann frühstücken wir ein Granita al limone. Ein fruchtiges Zitronensorbet und dazu gibt es ein Brioche und Espresso.%n%n" +
                "%n%nKomm vorbei und starte den Tag wie im Urlaub.%n%n" +
                "Wir freuen uns auf Dich!%nDein Team von Gelateria Giacomo%n", getName());
    }

    @Override
    public String getSubject() {
        return "Frühstücken wie in Sizilien?";
    }


}
