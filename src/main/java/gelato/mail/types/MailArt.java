package gelato.mail.types;

import gelato.mail.Mail;

abstract class MailArt {

    private String body;
    private String subject;
    private String covid;
    private String name;

    public MailArt(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getBody() {
        return body;
    }

    public String getSubject() {
        return subject;
    }

    public Mail generateMail() {

        return new Mail(getSubject(), getBody()+covid);
    }

    private void covid() {
        covid = "\n\nPS Wir haben natürlich alles für die Hygiene getan. Unsere Tische haben einen Mindestabstand von 2.5m, wir haben Scheiben zwischen den Tischen und alle unsere Bedienungen tragen Masken.";
    }


}
