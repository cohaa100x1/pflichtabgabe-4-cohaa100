package gelato.mail;

import gelato.MarketingLogic;
import gelato.kunde.Kunde;

import java.io.IOException;

import static java.lang.ProcessBuilder.Redirect.INHERIT;
import static java.util.concurrent.TimeUnit.SECONDS;

public class MailSending implements MailServer {

    private final MarketingLogic ml;

    public MailSending(MarketingLogic marketingLogic) {
        this.ml = marketingLogic;
    }

    @Override
    public void sendMail(Kunde kunde) {
        Mail mail = ml.createMail(kunde.getName());
        ProcessBuilder processBuilder = new ProcessBuilder();
        Process process = null;
        try {
            process = processBuilder
                    .command("java", "-jar", "mail.jar", kunde.getEmail(), mail.getSubject(), mail.getBody())
                    .redirectOutput(INHERIT)
                    .redirectError(INHERIT)
                    .start();
            process.waitFor(5, SECONDS);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}