package gelato;

import gelato.datenbank.IDatenbank;
import gelato.datenbank.Datenbank;
import gelato.kunde.Kunde;
import gelato.wetter.Wetterstation;

import java.util.List;

public class Main {
    private static final double CORONA_FACTOR = 0.13;
    public static final int SECHS_STUNDEN = 6 * 3600 * 1000;

    public static void main(String[] args) throws InterruptedException {

        System.out.println("Marketing Start");

        IDatenbank derby = new Datenbank(new Wetterstation());


        while (true) {
            System.out.println("Starte Marketing.");
            List<Kunde> kunden = derby.getKunden();
            for (Kunde k : kunden) {
                if (k.isReadyToReceiveMail() && Math.random() < CORONA_FACTOR) {
                    //Interface xyz
                    //art.getBody art.getSubject
                    derby.sendMarketingMail(k);
                }
            }

            System.out.println("Fertig! Schlafe fuer 6 Stunden");
            Thread.sleep(SECHS_STUNDEN);
        }
    }
}