package gelato.datenbank;

import gelato.kunde.Kunde;
import gelato.mail.MailSending;
import gelato.mail.MailServer;
import gelato.MarketingLogic;
import gelato.wetter.IWetterstation;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Datenbank implements IDatenbank {

    private Connection connection;
    private final IWetterstation wetterstation;
    private final MarketingLogic marketingLogic;

    public Datenbank(IWetterstation wetterstation) {
        try {
            this.connection = DriverManager.getConnection("jdbc:derby:kunden");
        } catch (SQLException throwables) {
            System.err.println("Datenbankverbindung konnte nicht hergestellt werden.");
            System.exit(-1);
        }
        this.wetterstation = wetterstation;
        marketingLogic = new MarketingLogic(wetterstation);
    }

    @Override
    public void updateLastMailDate(Kunde kunde) {
        try {
            PreparedStatement stmt = connection.prepareStatement("UPDATE kunden SET lastmail = CURRENT_DATE WHERE id = ?");
            stmt.setLong(1, kunde.getId());
            stmt.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public List<Kunde> getKunden() {
        ArrayList<Kunde> kunden = new ArrayList<>();
        String sql = "SELECT * FROM kunden";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String email = resultSet.getString("mail");
                Date date = resultSet.getDate("lastmail");
                Kunde kunde = new Kunde(id, name, email, date.toLocalDate());
                kunden.add(kunde);
            }
        } catch (SQLException throwables) {
            System.err.println("Fehler beim Laden der Kundendaten");
            System.exit(-1);
        }
        return kunden;
    }

    public void sendMarketingMail(Kunde kunde) {
        MailServer m = new MailSending(marketingLogic);
        updateLastMailDate(kunde);
        m.sendMail(kunde);
    }
}