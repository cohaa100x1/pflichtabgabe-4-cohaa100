package gelato.datenbank;

import gelato.kunde.Kunde;

import java.util.List;

public interface IDatenbank {

    void updateLastMailDate(Kunde kunde);

    List<Kunde> getKunden();

    void sendMarketingMail(Kunde kunde);

}
